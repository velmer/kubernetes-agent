# kubernetes-agent

## About

This is a [configuration repository example project](https://docs.gitlab.com/ee/user/clusters/agent/#defining-a-configuration-repository) of the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/).

## Setup

To use this project correctly, please follow the entire setup example in the [GitLab Kubernetes Agent documentation](https://docs.gitlab.com/ee/user/clusters/agent/) .

The minimal configuration repository layout should look like this:

`.gitlab/agents/<agent-name>/config.yaml`

The `config.yaml` file contents should look like this:

```yaml
gitops:
  manifest_projects:
  - id: "path-to/your-awesome-project"
```

`manifest_projects` are projects that are synchronized via the Agent. You can have more than one manifest project.

In this project you will also find a `resources.yaml` file which is [used to install the agent into your cluster](https://docs.gitlab.com/ee/user/clusters/agent/#installing-the-agent-into-the-cluster). 